/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.service;

import com.example.demo.bean.BusinessBean;
import com.example.demo.model.Business;
import com.example.demo.repository.BusinessRepository;
import java.util.Optional;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author usertbs1
 */
@Log4j2
@Service
public class BusinessService {

    @Autowired
    private BusinessRepository businessRepository;

    public void deleteBusiness(Long id) {

        Optional<Business> enterpriseOptional = businessRepository.findById(id);
        if (enterpriseOptional.isPresent()) {

            businessRepository.deleteById(id);

            log.info("La eliminación fue satisfactoria.");

        } else {
            log.error("No existe la Empresa a eliminar.");
        }
    }

    public Business getBusiness(Long id) {

        if (id != null) {
            Optional<Business> enterpriseOptional = businessRepository.findById(id);

            if (enterpriseOptional.isPresent()) {
                return enterpriseOptional.get();
            }

        } else {
            log.error("El id no es válido.");
        }

        return null;
    }

    public void insertBusiness(BusinessBean businessBean) {

        if (businessBean != null) {
            Business business = new Business();
            business.setName(businessBean.getName());
            business.setNit(businessBean.getNit());
            business.setCreationDate(businessBean.getCreationDate());
            business.setAdress(businessBean.getAdress());

            businessRepository.save(business);

            log.info("Se creó una nueva Empresa.");
        } else {
            log.error("No es posible crear una nueva Empresa.");
        }
    }    
    
    @Transactional
    public void updateBusiness(Long id, BusinessBean businessBean) {

        Optional<Business> businessOptional = businessRepository.findById(id);

        if (businessOptional.isPresent()) {

            businessRepository.updateBusiness(id, businessBean.getName(), businessBean.getNit(), businessBean.getCreationDate(), businessBean.getAdress());

            log.info("Actualización satisfactoria.");

        } else {
            log.error("No existe la Empresa a modificar.");
        }

    }
}
