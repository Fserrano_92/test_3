/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.bean;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;


/**
 *
 * @author usertbs1
 */


@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class BusinessBean implements Serializable {
    
    @Expose
    private Long id;
    
    @Expose
    private String name;
    
    @Expose
    private Integer nit;

    @Expose
    private String adress;

    @Expose
    private Date creationDate;
}
