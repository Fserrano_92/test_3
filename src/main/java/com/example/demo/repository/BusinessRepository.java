/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.repository;

import com.example.demo.model.Business;
import java.util.Date;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author usertbs1
 */
public interface BusinessRepository extends CrudRepository<Business, Long> {
    
    @Transactional
    @Modifying
    @Query("UPDATE Business b SET b.name = :name, b.nit = :nit, b.creationDate = :creationDate, b.adress = :adress WHERE b.id = :id")
    void updateBusiness(@Param("id") Long id, @Param("name") String name, @Param("nit") Integer nit, @Param("creationDate") Date creationDate, @Param("adress") String adress);
}
