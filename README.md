# Rutas 

## Para obtener una empresa por id: 
http://localhost:8090/service/business/{id}

## Para crear una empresa: 
http://localhost:8090/service/business/

## Para modificar una empresa por su id tomando en cuenta que es un metodo put: 
http://localhost:8090/service/business/{id}

## Para eliminar una empresa por su id tomando en cuenta que es un metodo detele: 
http://localhost:8090/service/business/{id}

# QUERYS

CREATE TABLE TEST_BUSINESS (
    TB_ID NUMBER(20) NOT NULL,
    TB_NAME VARCHAR2(80) NULL,
    TB_NIT NUMBER(20) NULL,
    TB_ADRESS VARCHAR2(240) NULL,
    TB_CDATE TIMESTAMP NULL
);

CREATE SEQUENCE TEST_BUSINESS_SEQ
MINVALUE 1
MAXVALUE 9999999999999999
START WITH 1
INCREMENT BY 1
NOCACHE
NOCYCLE;

ALTER TABLE TEST_BUSINESS ADD CONSTRAINT PK_NUSINESS_01  PRIMARY KEY (TB_ID);



