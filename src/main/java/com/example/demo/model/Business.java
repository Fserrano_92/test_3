/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.model;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author usertbs1
 */



@Entity
@Table(name = "TEST_BUSINESS")
@Getter
@Setter
@ToString
public class Business implements Serializable {
    
    @Id
    @Expose
    @Column(name = "TB_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_BUSINESS_SEQ")
    @SequenceGenerator(sequenceName = "TEST_BUSINESS_SEQ", allocationSize = 1, name = "TEST_BUSINESS_SEQ")
    private Long id;
        
    @Expose
    @Column(name = "TB_NAME")
    private String name;
            
    @Expose
    @Column(name = "TB_NIT")
    private Integer nit;
    
    @Expose
    @Column(name = "TB_ADRESS")
    private String adress;
    
    @Expose
    @Column(name = "TB_CDATE")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date creationDate;
    
    
}
