/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.controller;

import com.example.demo.bean.BusinessBean;
import com.example.demo.service.BusinessService;
import com.google.gson.GsonBuilder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author usertbs1
 */

@Api
@Log4j2
@RestController
@RequestMapping("/service/business")
public class BusinessController {
    
    @Autowired
    private GsonBuilder gsonBuilder;
    
    @Autowired
    private BusinessService businessService;
    
    
    @PostMapping(value = "/")
    public ResponseEntity<String> createEnterprise(@RequestBody(required = true) BusinessBean businessBean) {
        HttpStatus hs = HttpStatus.OK;

        try {
            businessService.insertBusiness(businessBean);
            return new ResponseEntity<>(gsonBuilder.create().toJson("creation.success"), hs);
        } catch (Exception e) {
            log.error("Error detectado: ", e);

            hs = HttpStatus.INTERNAL_SERVER_ERROR;
            return new ResponseEntity<>(gsonBuilder.create().toJson(ExceptionUtils.getFullStackTrace(e)), hs);
        }
    }
    
    @GetMapping(value="/{id}")
    public ResponseEntity<String> readEnterprise(@PathVariable(value="id") Long id){
        HttpStatus hs = HttpStatus.OK;
        
        try {
            return new ResponseEntity<>(gsonBuilder.create().toJson(businessService.getBusiness(id)), hs);
        } catch (Exception e) {
            log.error("Error detectado: ", e);
            return new ResponseEntity<>(gsonBuilder.create().toJson(ExceptionUtils.getFullStackTrace(e)), hs);
        }
    }
    
    @PutMapping(value = "/{id}")
    public ResponseEntity<String> updateEnterprise(@PathVariable("id") Long id,
            @ApiParam(value = "EnterpriseBean", required = true) @RequestBody(required = true) BusinessBean businessBean) {
        HttpStatus hs = HttpStatus.OK;

        try {
            businessService.updateBusiness(id, businessBean);
            return new ResponseEntity<>(gsonBuilder.create().toJson("update.success"), hs);
        } catch (Exception e) {
            log.error("Error detectado: ", e);

            hs = HttpStatus.INTERNAL_SERVER_ERROR;
            return new ResponseEntity<>(gsonBuilder.create().toJson(ExceptionUtils.getFullStackTrace(e)), hs);
        }
    }
    
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> deleteEnterprise(@PathVariable("id") Long id) {
        HttpStatus hs = HttpStatus.OK;

        try {
            businessService.deleteBusiness(id);
            return new ResponseEntity<>(gsonBuilder.create().toJson("delete.success"), hs);
        } catch (Exception e) {
            log.error("Error detectado: ", e);

            hs = HttpStatus.INTERNAL_SERVER_ERROR;
            return new ResponseEntity<>(gsonBuilder.create().toJson(ExceptionUtils.getFullStackTrace(e)), hs);
        }
    }
}

